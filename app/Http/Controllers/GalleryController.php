<?php

namespace App\Http\Controllers;

use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    public function store(Request $request )
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png|required',
            'description' => 'required',

        ]);

        $gallery=new Gallery();
        $gallery->category=$request->category;
         $gallery->description=$request->description;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location ='assest/images/gallery/' . $name;

            Image::make($file)->save($location);
            $gallery->image=$name;
        }
        $gallery->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/gallery/view');
    }
    public function view()
    {
        $gallerys=Gallery::orderBy('created_at','desc')->paginate(10);
        return view('admin.gallery.view',compact('gallerys'));
    }

    public function delete($id)
    {
        $gallerys=Gallery::findOrfail($id);

        \File::delete('assest/images/gallery/'.$gallerys->image);
        $gallerys->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/gallery/view');
    }
    public function edit($id)
    {
        $gallerys=Gallery::find($id);
        return view('admin.gallery.edit',compact('gallerys'));
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [

            'description' => 'required',
            'image'=>'|image|mimes:jpeg,png',


        ]);
        $gallerys=Gallery::find($id);
        $gallerys->category=$request->category;
        $gallerys->description=$request->description;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }

            $location ='assest/images/gallery/' . $name;

            Image::make($file)->save($location);
            $gallerys->image=$name;
        }
        $gallerys->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/gallery/view');
    }
}
