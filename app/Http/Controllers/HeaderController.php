<?php

namespace App\Http\Controllers;

use App\Header;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class HeaderController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png|required',
            'short_desc1' => 'required',
            'short_desc2'=>'required',

            'phone'=>'required',
            'email'=>'required',

        ]);
        $header = new Header();
        $header->short_desc1 = $request->short_desc1;
        $header->short_desc2= $request->short_desc2;
        $header->footer_heading= $request->footer_heading;
        $header->footer_description= $request->footer_description;
        $header->phone= $request->phone;
        $header->email= $request->email;

        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location ='assest/images/header/' . $name;

            Image::make($file)->encode('png')->save($location);
            $header->image=$name;
        }
        $header->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/header/view');
    }
    public function view()
    {
        $headers=Header::orderBy('created_at','desc')->paginate(10);
        return view('admin.header.view',compact('headers'));
    }
    public function delete($id)
    {
        $header=Header::findOrfail($id);
        \File::delete('assest/images/header/'.$header->image);

        $header->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/header/view');
    }
    public function edit($id)
    {
        $headers=Header::findOrfail($id);
        return view('admin.header.edit',compact('headers'));
    }
    public function update($id,Request $request)
    {

        $this->validate($request, [
            'short_desc1' => 'required',
            'short_desc2' => 'required',
           'image'=>'|image|mimes:jpeg,png',


        ]);
        $header=Header::findOrfail($id);
//        $joinme=Header::all()
//            ->join('settings')->get();
//        dd($joinme);
        $header->short_desc1=$request->short_desc1;
        $header->short_desc2=$request->short_desc2;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location ='assest/images/header/' . $name;

            Image::make($file)->encode('png')->save($location);
            $header->image=$name;
        }
        $header->footer_heading= $request->footer_heading;
        $header->footer_description= $request->footer_description;
        $header->phone= $request->phone;
        $header->email= $request->email;
        $header->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/header/view');
    }
}
