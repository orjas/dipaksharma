<?php

namespace App\Http\Controllers;

use Anhskohbo\NoCaptcha\Facades\NoCaptcha;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class MessageController extends Controller
{
    public function index()

    {
     $messages=Message::orderBy('created_at','desc')->paginate(10);
        return view('admin.message',compact('messages'));

    }

    public function delete($id)
    {
        Message::destroy($id);
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/message');
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'message'=>'required',
            'g-recaptcha-response' => 'required|captcha'
        ],
            [
                'g-recaptcha-response' => [
                    'required' => 'Please verify that you are not a robot.',
                    'captcha' => 'Captcha error! try again later or contact site admin.',
                ]]);

        $mess=new Message();
        $mess->name=$request->name;
        $mess->email=$request->email;
        $mess->message=$request->message;
        $mess->save();
        $request->session()->flash('alert-success', 'Message sent Successfully');
        return redirect()->back();

    }
}
