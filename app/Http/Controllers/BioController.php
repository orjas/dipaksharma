<?php

namespace App\Http\Controllers;

use App\Bio;
use App\Http\Requests\BioStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BioController extends Controller
{
    public function store(Request $request)
    {
       $this->validate($request,[
           'name' => 'required|min:6|max:255',
           'description' => 'required'
       ]);
        $bios = new Bio();
        $bios->name = $request->name;
        $bios->description = $request->description;
        $bios->date = $request->date;
        $bios->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/bio/view');
    }
    public function view()
    {


        $bios=Bio::orderBy('created_at','desc')->paginate(10);

        return view('admin.bio.view',compact('bios'));
    }
    public function delete($id)
    {
        $bios=Bio::findOrfail($id);



        $bios->delete();

        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/bio/view');
    }
    public function edit($id)
    {
        $bios=Bio::find($id);
        return view('admin.bio.edit',compact('bios'));
    }
    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:6|max:255',
            'description' => 'required',
            'date'=>'required',

        ]);
        $bios=Bio::find($id);
        $bios->name=$request->name;
        $bios->description=$request->description;
        $bios->date=$request->date;
        $bios->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/bio/view');
    }
}
