<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function view()
    {
        $setting=Setting::orderBy('created_at','desc')->paginate(10);
        return view('admin.setting.view',compact('setting'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',


        ]);
        $setting = new Setting();
        $setting->title = $request->title;
        $setting->facebook_page= $request->facebook_page;
            $setting->facebook_link=$request->facebook_link;
                $setting->youtube=$request->youtube;
                    $setting->twitter=$request->twitter;
            $setting->google=$request->google;
        $setting->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/setting/view');
    }
    public function delete($id)
    {
        $setting=Setting::findOrfail($id);

        $setting->delete();

        return redirect('/admin/setting/view');
    }

    public function edit()
    {
        $settings=Setting::first();


        return view('admin.setting.edit',compact('settings'));
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'title' => 'required',


        ]);
        $setting = Setting::findOrfail($id);
        $setting->title = $request->title;
        $setting->facebook_page= $request->facebook_page;
        $setting->facebook_link=$request->facebook_link;
        $setting->youtube=$request->youtube;
        $setting->twitter=$request->twitter;
        $setting->google=$request->google;
        $setting->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/setting/edit');
    }

}
