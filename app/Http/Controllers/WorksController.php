<?php

namespace App\Http\Controllers;

use App\Works;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class WorksController extends Controller
{
    public function store(Request $request )
    {
        $this->validate($request, [
            'name' => 'required|min:6|max:255',
            'description' => 'required|min:6',

            'image' => 'image|mimes:jpeg,png|required',
        ]);

      $work=new Works();
        $work->name=$request->name;
        $work->short_desc="";
        $work->description=$request->description;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location ='assest/images/works/' . $name;

            Image::make($file)->resize(450,300)->encode('png')->save($location);
            $work->image=$name;
        }
        $work->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/works/view');
    }
    public function view()
    {
        $works=Works::orderBy('created_at','desc')->paginate(10);
        return view('admin.works.view',compact('works'));
    }

    public function delete($id)
    {
        $works=Works::findOrfail($id);

        \File::delete('assest/images/works/'.$works->image);
        $works->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/works/view');
    }
    public function edit($id)
    {
        $works=Works::find($id);
        return view('admin.works.edit',compact('works'));
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:6|max:255',
            'description' => 'required|min:6',


            'image'=>'|image|mimes:jpeg,png',

        ]);
        $works=Works::find($id);


        $works->name=$request->name;
        $works->description=$request->description;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location ='assest/images/works/' . $name;

            Image::make($file)->resize(450,300)->encode('png')->save($location);
            $works->image=$name;
        }
        $works->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/works/view');
    }
}
