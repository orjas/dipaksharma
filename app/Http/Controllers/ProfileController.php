<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    public function view()
    {
        $profile=Profile::first();
        return view('admin.profile.view',compact('profile'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'description'=>'required',
        ]);

        $profile = new Profile();
        $profile->description=$request->description;

        $profile->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/profile/view');
    }
    public function delete($id)
    {
        $profile=Profile::findOrfail($id);

        $profile->delete();

        return redirect('/admin/profile/view');
    }

    public function edit()
    {
        $profile=Profile::first();


        return view('admin.profile.edit',compact('profile'));
    }

    public function update($id,Request $request)
    {
        $this->validate($request,[
            'description'=>'required',
        ]);

        $profile = Profile::findOrfail($id);
        $profile->description=$request->description;

        $profile->save();
        $request->session()->flash('alert-success', 'Successful updated!');

        return redirect('admin/profile/edit');
    }
}
