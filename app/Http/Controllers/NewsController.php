<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    //
    public function store(Request $request )
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required|min:6',


            'date'=>'required',
            'url'=>'required'
        ]);
        $news=new News();
        $news->title=$request->title;
        $news->url=$request->url;

        $news->description=$request->description;

        $news->date=$request->date;

        $news->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('admin/news');
    }
    public function view()
    {
        $newses=News::orderBy('date','desc')->paginate(10);
        return view('admin.news.view',compact('newses'));
    }

    public function delete($id)
    {
        $news=News::findOrfail($id);


        $news->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/news');
    }
    public function edit($id)
    {
        $news=News::find($id);
        return view('admin.news.edit',compact('news'));
    }

    public function update($id,Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required|min:6',
            'date'=>'required',
            'url'=>'required'



        ]);

        $news=News::findOrfail($id);

        $news->title=$request->title;
        $news->url=$request->url;

        $news->description=$request->description;

        $news->date=$request->date;
        $news->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/news');
    }
}
