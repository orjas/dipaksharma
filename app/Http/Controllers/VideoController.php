<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VideoController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'video' => 'required',

        ]);
        $videos = new Video();
        $videos->title=$request->title;
        $videos->category = $request->category;
        $videos->url=$request->video;
        $url = $request->video;
        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);
        $id = $matches[1];
        $videos->video=$id;
        $videos->save();
        $request->session()->flash('alert-success', 'Successful Added!');

        return redirect('/admin/video/view');
    }
    public function view()
    {
        $videos=Video::orderBy('created_at','desc')->paginate(10);
        return view('admin.video.view',compact('videos'));
    }
    public function delete($id)
    {
        $videos=Video::findOrfail($id);

        $videos->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/video/view');
    }
    public function edit($id)
    {
        $videos=Video::findOrfail($id);
        return view('admin.video.edit',compact('videos'));
    }
    public function update($id,Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'video' => 'required',

        ]);
        $videos=Video::findOrfail($id);
        $videos->title=$request->title;

        $videos->category=$request->category;
        $videos->url=$request->video;
        if($request->video){
            $url = $request->video;
            preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $url, $matches);

            $id = $matches[1];
            $videos->video=$id;

        }

         $videos->save();
        $request->session()->flash('alert-success', 'Successful Updated!');

        return redirect('admin/video/view');
    }
}
