<?php

namespace App\Http\Controllers;

use App\Testomonial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TestomonialController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:6|max:255',
            'message' => 'required|min:6',
            'position' => 'required',
            'link'=>'required',
            'image' => 'image|mimes:jpeg,png|required',
        ]);
       $test=new Testomonial();
        $test->name=$request->name;
        $test->position=$request->position;
        $test->message=$request->message;
        $test->link=$request->link;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location = 'assest/images/testomonial/' . $name;

            Image::make($file)->resize(110,110)->encode('png')->save($location);
           $test->image=$name;
        }
        $test->save();
        $request->session()->flash('alert-success', 'Successful added!');
        return redirect('admin/testomonial/view');
    }

    public function view()
    {
        $test=Testomonial::orderBy('created_at','desc')->paginate(10);
        return view('admin.testomonial.view',compact('test'));
    }

    public function delete($id)
    {
        $test=Testomonial::findOrfail($id);

        \File::delete('assest/images/testomonial/'.$test->image);
        $test->delete();
        \Illuminate\Support\Facades\Request::session()->flash('alert-success', 'Delete Successfully!');

        return redirect('/admin/testomonial/view');
    }

    public function edit($id)
    {
        $test=Testomonial::find($id);
        return view('admin.testomonial.edit',compact('test'));
    }


    public function update($id,Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:6|max:255',
            'message' => 'required|min:6',
            'position' => 'required',
            'link'=>'required',
            'image'=>'|image|mimes:jpeg,png',


        ]);
$test=Testomonial::find($id);
        $test->name=$request->name;
        $test->position=$request->position;
        $test->message=$request->message;
        $test->link=$request->link;
        if($request->hasFile('image')){
            $file=$request->image;
            $ext=$file->getClientOriginalExtension();


            $name=str_replace(" ","",$file->getClientOriginalName());

            if(strlen($name)>15){
                $output=substr($name,0,10);
                $name=$output.'.'.$ext;
            }else if(strlen($name)<=15){
                $name=rand(1000,9999).$name;
            }
            $location = 'assest/images/testomonial/' . $name;

            Image::make($file)->resize(110,110)->encode('png')->save($location);
            $test->image=$name;
        }
$test->save();
        $request->session()->flash('alert-success', 'Successful Updated!');
        return redirect('admin/testomonial/view');
}
}
