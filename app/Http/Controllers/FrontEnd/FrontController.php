<?php

namespace App\Http\Controllers\FrontEnd;

use App\Bio;
use App\Gallery;
use App\Header;
use App\News;
use App\Profile;
use App\Setting;
use App\Testomonial;
use App\Video;
use App\Works;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;

class FrontController extends Controller
{
    public function bio()
    {

        $bios=Bio::all()
        ->sortByDesc('created_at');


//            ->orderBy('created_at', 'desc')
//        ->get();

        return view('official.bio',compact('bios'));
    }
    public function gallery(){
        $gallerys=Gallery::where('category','Gallery')->orderBy('created_at','DESC')->paginate(30);
        return view('official.gallery',compact('gallerys'));

    }

    public function video(){
        $videos=Video::where('category','Gallery')->orderBy('created_at','DESC')->paginate(30);
        return view('official.video',compact('videos'));

    }

    public function works()
    {
        $works=Works::all()->sortByDesc('created_at');

        return view('official.works',compact('works'));
    }

    public function home()
    {
        $headers=Header::all()->sortByDesc('created_at')->first();

        $videos=Video::orderBy('created_at','DESC')->limit(3)->get();

        $works=Works::orderBy('created_at','DESC');
        $gallerys=Gallery::orderBy('created_at','DESC')->limit(6)->get();
        $testomonials=Testomonial::orderBy('created_at','DESC')->limit(3)->get();

        return view('official.home',compact('headers','videos','works','gallerys','testomonials'));


    }
    public function contact()
    {
        $setting=Setting::first();
        return view('official.contact',compact('setting'));
    }

    public function profile()
    {
        $profile=Profile::first();
        $header=Header::first();
        return view('official.profile',compact('profile','header'));
    }

    public function news()
    {
        $news=News::orderBy('date','DESC')->paginate(10);
        return view('official.news',compact('news'));
}
    public function interview(){
        $videos=Video::where('category','Interview')->orderBy('created_at','DESC')->paginate(20);
        return view('official.interview',compact('videos'));

    }
    public function images(){
        $gallerys=Gallery::where('category','News')->orderBy('created_at','DESC')->paginate(30);
        return view('official.newsgallery',compact('gallerys'));

    }
}
