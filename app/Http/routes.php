<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/', function () {
//    return view('official.home');
//});
Route::get('/','FrontEnd\FrontController@home')->name('/');
Route::get('bio','FrontEnd\FrontController@bio')->name('bio');
Route::get('gallery/image','FrontEnd\FrontController@gallery')->name('photo');
Route::get('gallery/video','FrontEnd\FrontController@video')->name('video');


Route::get('/media/interview','FrontEnd\FrontController@gallery')->name('interview');
Route::get('/media/news','FrontEnd\FrontController@video')->name('news');
Route::get('/media/jpt','FrontEnd\FrontController@gallery')->name('jpt');

Route::get('/works','FrontEnd\FrontController@works')->name('works');
Route::get('/contact','FrontEnd\FrontController@contact')->name('contact');
Route::get('/profile','FrontEnd\FrontController@profile')->name('profile');
Route::get('/media/news','FrontEnd\FrontController@news')->name('news');
Route::get('/media/interview','FrontEnd\FrontController@interview')->name('interview');
Route::get('/media/images','FrontEnd\FrontController@images')->name('images');




Route::post('/contact','MessageController@store');
Route::get('/home',function(){
   return redirect( '/admin/header/view');
});

//Auth::routes();
//Route::get('/login','Auth\LoginController@showLoginForm' )->name('login');
//Route::post('/login','Auth\LoginController@login' );
//Route::post('/logout','Auth\LoginController@logout')->name('logout');

Route::get('dashboard/login', 'Auth\AuthController@getLogin')->name('login');
Route::post('dashboard/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('logout');



Route::group(['middleware'=>'auth'], function () {



Route::get('admin/dashboard',function(){
    return view('admin.dashboard');
});


//Testomonial Routes
Route::get('/admin/testomonial/add',function(){
    return view('admin.testomonial.add');
});
Route::post('/admin/testomonial/add','TestomonialController@store');
Route::get('/admin/testomonial/view','TestomonialController@view');
Route::get('/admin/testomonial/{id}/delete','TestomonialController@delete');
Route::get('/admin/testomonial/{id}/edit','TestomonialController@edit');
Route::post('/admin/testomonial/{id}/update','TestomonialController@update');

//works route
Route::get('/admin/works/add',function(){
    return view('admin.works.add');
});
Route::post('/admin/works/add','WorksController@store');
Route::get('/admin/works/view','WorksController@view');
Route::get('/admin/works/{id}/delete','WorksController@delete');
Route::get('/admin/works/{id}/edit','WorksController@edit');
Route::post('/admin/works/{id}/update','WorksController@update');

//bio routes
Route::get('/admin/bio/add',function(){
    return view('admin.bio.add');
});
Route::post('/admin/bio/add','BioController@store');
Route::get('/admin/bio/view','BioController@view');
Route::get('/admin/bio/{id}/delete','BioController@delete');
Route::get('/admin/bio/{id}/edit','BioController@edit');
Route::post('/admin/bio/{id}/update','BioController@update');

//gallery routes
Route::get('/admin/gallery/add',function(){
    return view('admin.gallery.add');
});
Route::post('/admin/gallery/add','GalleryController@store');
Route::get('/admin/gallery/view','GalleryController@view');
Route::get('/admin/gallery/{id}/delete','GalleryController@delete');
Route::get('/admin/gallery/{id}/edit','GalleryController@edit');
Route::post('/admin/gallery/{id}/update','GalleryController@update');

//video routes
Route::get('/admin/video/add',function(){
    return view('admin.video.add');
});
Route::post('/admin/video/add','VideoController@store');
Route::get('/admin/video/view','VideoController@view');
Route::get('/admin/video/{id}/delete','VideoController@delete');
Route::get('/admin/video/{id}/edit','VideoController@edit');
Route::post('/admin/video/{id}/update','VideoController@update');


    //message
    Route::get('/admin/message','MessageController@index');
    Route::get('/admin/{id}/message','MessageController@delete');

    //header route
    Route::get('/admin/header/add',function(){
        return view('admin.header.add');
    });
    Route::post('/admin/header/add','HeaderController@store');
    Route::get('/admin/header/view','HeaderController@view');
    Route::get('/admin/header/{id}/delete','HeaderController@delete');
    Route::get('/admin/header/{id}/edit','HeaderController@edit');
    Route::post('/admin/header/{id}/update','HeaderController@update');

    //setting Route

    Route::get('/admin/setting/add',function(){
        return view('admin.setting.add');
    });
    Route::post('/admin/setting/add','SettingController@store');
    Route::get('/admin/setting/view','SettingController@view');
    Route::get('/admin/setting/{id}/delete','SettingController@delete');
    Route::get('/admin/setting/edit','SettingController@edit');
    Route::post('/admin/setting/{id}/update','SettingController@update');


    //profile
    Route::get('/admin/profile/add',function(){
        return view('admin.profile.add');
    });
    Route::post('/admin/profile/add','ProfileController@store');
    Route::get('/admin/profile/view','ProfileController@view');
    Route::get('/admin/profile/{id}/delete','ProfileController@delete');
    Route::get('/admin/profile/edit','ProfileController@edit');
    Route::post('/admin/profile/{id}/update','ProfileController@update');

//Admin profile
    Route::get('admin/adminprofile',function(){
        return view('admin.adminprofile.edit');
    });
    Route::post('admin/adminprofile/update','AdminProfileController@update');

    //news
    Route::get('/admin/news/add',function(){
        return view('admin.news.add');
    });
    Route::post('/admin/news/add','NewsController@store');
    Route::get('/admin/news','NewsController@view');
    Route::get('/admin/news/{id}/delete','NewsController@delete');
    Route::get('/admin/news/{id}/edit','NewsController@edit');
    Route::post('/admin/news/{id}/update','NewsController@update');

});
