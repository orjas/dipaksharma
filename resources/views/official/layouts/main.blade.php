<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{$title->title}}|@yield('title')</title>


    <meta name="author" content="{{$title->title}}">
    <meta property="og:url" content="{{url('/')}}" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:title" content="{{$title->title}}"/>
    <meta property="og:image" content="{{ asset('assest/images/header/'.$shareImage->image) }}"/>
    <meta property="og:site_name" content="{{$title->title}}"/>
    <meta property="og:description" content="{{$title->title}}"/>

    <link rel="canonical" href="{{url('/')}}" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="{{"@".$title->title}}" />
    <meta name="twitter:creator" content="{{"@".$title->title}}" />
    <meta property="og:url" content="{{url('/')}}" />
    <meta property="og:title" content="{{$title->title}}" />
    <meta property="og:description" content="{{$title->title}}" />
    <meta property="og:image" content="{{ asset('assest/images/header/'.$shareImage->image) }}" />

    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">


    @include('official.includes.head')
    {{--@include('Admin.layouts.addstyles')--}}
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body id="container-fluid">
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11&appId=1423581781088072';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<?php $message='';?>
<?php $message= Session::get('alert-success');?>



@yield('section')

@include('official.includes.script')
{{--@include('Admin.layouts.addscripts')--}}
<script>
var success="{{$message}}";

if(success!=='') {
$.notify(
success,
        {
            className:'success',
            globalPosition: 'top center'}
);
}
</script>
@yield('Myscript')

</body>

</html>
