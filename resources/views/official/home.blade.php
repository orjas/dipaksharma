@extends('official.layouts.main')


@section('section')
    <style>
        header.masthead{
            background-image: url("{{ asset('assest/images/header/'.$headers->image) }}") !important;
        }



    </style>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="mainNav" >
        @include('official.includes.nav')

    </nav>

    <header class="masthead text-center text-white d-flex">
        <div class="container my-auto">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <h1 class="text-uppercase">
                        <br>
                        <strong style=" text-shadow: 1px 2px 5px #000000;" >{{$headers->short_desc1}}</strong>
                    </h1>
                    <hr>
                </div>
                <div class="col-lg-8 mx-auto ">

                    <p class="text-faded mb-5" >
                        <div style="  text-shadow: 1px 1px 2px black;">{{$headers->short_desc2}}</div></p>
                    <a class="btn btn-primary btn-xl js-scroll-trigger" href="{{url('/profile')}}">Find More About Me</a>
                </div>
            </div>
        </div>
    </header>

    <section  class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="testimonial-slider" class="owl-carousel">
                       @foreach($testomonials as $test)
                        <div class="testimonial">
                            <div class="pic">
                              <a href="{{$test->link}}" target="_blank">  <img src="{{asset('assest/images/testomonial/'.$test->image)}}" width="100px" height="100px"></a>
                            </div>
                            <p class="description">
                                {!! $test->message !!}
                            </p>
                            <div class="testimonial-profile">
                                <h3 class="title"> {{$test->name}} -</h3>
                                <span class="post"> {{$test->position}}</span>
                            </div>
                        </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>

    </section>

    <section id="services">
        <div class="container ">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><i class="fa fa-video-camera"></i>Latest Video </h2>
                    <hr class="my-4">
                </div>
            </div>
        </div>
        <div class="container ">
            <div class="row">
                @foreach($videos as $video)
                <div class="col-sm-4">
                    <iframe width="100%" height="200" src="https://www.youtube.com/embed/{{$video->video}}" frameborder="0" gesture="media" allowfullscreen></iframe>

                </div>
                @endforeach

            </div>
        </div>
        <div class="col-md-12 text-center">
            <a class="btn  btn-primary btn-xl js-scroll-trigger " href="{{route('video')}}">More Videos</a>
        </div>
    </section>


    <section class="p-0" id="portfolio">
        <div class="container-fluid p-0">
            <div class="row no-gutters popup-gallery">
                <div class="container ">
                    <div class="row">
                @foreach($gallerys as $gallery)
                <div class="mt-2 col-sm-4 ">
                    <a class="portfolio-box" href="{{url(asset('assest/images/gallery/'.$gallery->image))}}">
                        <img  src="{{url(asset('assest/images/gallery/'.$gallery->image))}}" width="350" height="200" alt="">
                        {{--<div class="portfolio-box-caption">--}}
                            {{--<div class="portfolio-box-caption-content">--}}
                                {{--<div class="project-category text-faded">--}}

                                {{--</div>--}}
                                {{--<div class="project-name">--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </a>
                </div>
                @endforeach
</div>
                    </div>
            </div>
        </div>
        <div class=" mt-2 col-md-12 text-center">
            <a class="btn  btn-primary btn-xl js-scroll-trigger " href="{{route('photo')}}">Gallery</a>
        </div>
    </section>



    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <h2 class="section-heading">{{$headers->footer_heading}}</h2>
                    <hr class="my-4">
                    <p class="mb-5">{!!  $headers->footer_description!!}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 ml-auto text-center">
                    <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                    <p>
                        <a href="tel:{{$headers->phone}}" target="_blank">{{$headers->phone}}</a>

                        </p>
                </div>
                <div class="col-lg-4 mr-auto text-center">
                    <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                    <p>
                        <a href="mailto:{{$headers->email}}" target="_blank">{{$headers->email}}</a>
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection

