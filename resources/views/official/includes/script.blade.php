
<!-- Bootstrap core JavaScript -->
<script src="{{asset('assest/js/jquery.min.js')}}"></script>
<script src="{{asset('assest/js/bootstrap.bundle.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{asset('assest/js/jquery.easing.min.js')}}"></script>
<script src="{{asset('assest/js/scrollreveal.min.js')}}"></script>
<script src="{{asset('assest/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assest/js/notify.js')}}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
<!-- Custom scripts for this template -->
<script src="{{asset('js/creative.min.js')}}"></script>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.min.js"></script>--}}

<script>
    // Magnific popup calls
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0, 1]
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
        }
    });
</script>