
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">


<!-- Bootstrap core CSS -->
<link href="{{asset('assest/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assest/css/slashplusMenu.css')}}" rel="stylesheet">


<!-- Custom fonts for this template -->
<link href="{{asset('assest/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
<!-- Plugin CSS -->
<link href="{{asset('assest/css/magnific-popup.css')}}" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="{{asset('assest/css/creative.min.css')}}" rel="stylesheet">
<link href="{{asset('assest/css/style.css')}}" rel="stylesheet">
{{--<link rel="stylesheet" href="{{asset('scss/creative.scss')}}">--}}
<style>
    #exceptHomeNav .navbar-nav>li.nav-item>a.nav-link, #exceptHomeNav .navbar-nav>li.nav-item>a.nav-link:focus, #mainNav .navbar-nav>li.nav-item>a.nav-link, #mainNav .navbar-nav>li.nav-item>a.nav-link:focus {
        text-shadow: 1px 2px 5px black;
    }
    #exceptHomeNav.navbar-shrink .navbar-nav>li.nav-item>a.nav-link, #exceptHomeNav.navbar-shrink .navbar-nav>li.nav-item>a.nav-link:focus, #mainNav.navbar-shrink .navbar-nav>li.nav-item>a.nav-link, #mainNav.navbar-shrink .navbar-nav>li.nav-item>a.nav-link:focus {
        text-shadow: 1px 2px 5px white;
    }
  .navbar-nav li.active {

        background-color:#f05f40 ;

    }
    .dropdown-item.active, .dropdown-item:active {
        color: #fff;
        text-decoration: none;
        background-color: #f05f40;
    }
</style>



