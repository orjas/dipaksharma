<div class="container">
    <!--<a class="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a>-->
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse " id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="{{route('/')}}" >Home</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle {!! Request::segment(1)=='gallery'?'active':' '!!}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gallery
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    <a class="dropdown-item {!! Request::segment(2)=='video'?'active':' '!!}" href="{{route('video')}}"> Video  </a>

                    <a class="dropdown-item {!! Request::segment(2)=='image'?'active':' '!!}" href="{{route('photo')}}"> Images </a>


                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle {!! Request::segment(1)=='media'?'active':' '!!}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Media
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    <a class="dropdown-item {!! Request::segment(2)=='interview'?'active':' '!!}" href="{{route('interview')}}"> Interview  </a>

                    <a class="dropdown-item {!! Request::segment(2)=='news'?'active':' '!!}" href="{{route('news')}}"> News </a>
                    <a class="dropdown-item {!! Request::segment(2)=='images'?'active':' '!!}" href="{{route('images')}}"> Images </a>


                </div>
            </li>




            <li class="nav-item ">
                <a class="nav-link js-scroll-trigger  {!! Request::segment(1)=='works'?'active':' '!!}" href="{{route('works')}}">Works </a>
            </li>


            {{--<li class="nav-item">--}}
                {{--<a class="nav-link js-scroll-trigger  {!! Request::segment(1)=='bio'?'active':' '!!}" href="{{route('bio')}}">Bio</a>--}}

            {{--</li> --}}
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger  {!! Request::segment(1)=='profile'?'active':' '!!}" href="{{route('profile')}}">Profile</a>
            </li>

            <li class="nav-item">
                <a class="nav-link js-scroll-trigger contact  {!! Request::segment(1)=='contact'?'active':' '!!}" href="{{route('contact')}}">Contact</a>
            </li>



        </ul>
    </div>
</div>
