@extends('official.layouts.main')
@section('title')
    Works
@endsection
@section('section')

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>

    <section id="works">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"> Find my works</h2>
                    <hr class="my-4">
                </div>
                @foreach($works as $work)
                <div class="col-sm-6 col-md-6 col-lg-6 mt-4">
                    <div class="card card-inverse card-info">
                        <img class="" src="{{asset('assest/images/works/'.$work->image)}}" height="400">
                        <div class="card-block">
                            <figure class="profile profile-inline">
                                <img src="http://success-at-work.com/wp-content/uploads/2015/04/free-stock-photos.gif" class="profile-avatar" alt="">
                            </figure>
                            <h4 class="card-title">{{$work->name}}</h4>
                            <div class="card-text">
                              <div class="item{{$work->id}}">
                                  {!!  str_limit(($work->description), 100) !!}

                                @if (strlen(strip_tags($work->description)) > 100)
                                    <a  class="btn btn-info btn-sm" onclick="showMe({{$work->id}})">show More</a>
                                @endif
                                  </div>

                                <div class="more_Item{{$work->id}} " hidden="true" >{!! ( $work->description )!!}
                              </div>
                        </div>
                    </div>
                </div>
                </div>
                    @endforeach

        </div>
            </div>
    </section>
@endsection

@section('Myscript')
    <script>
        function showMe(id){
            $('div.item'+id).hide();
            $('div.more_Item'+id).removeAttr('hidden');
        }


    </script>
    @endsection
