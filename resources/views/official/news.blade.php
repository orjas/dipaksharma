@extends('official.layouts.main')
@section('title')
    News
    @endsection
@section('section')

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>
<?php $count=0;?>
    <section id="bio">
    <div class="container news-page">
        <div class="page-header">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><i class="fa fa-clock-o"></i>Latest News & Events </h2>
                <hr class="my-4">

            </div>
        </div>





            <div class="row">


           @foreach($news as $new)
                <div class="col-md-12 mb-20">

                    <h4>{{$new->title}}&nbsp;<span style="font-size: 14px;text-decoration: none !important;">{{$new->date}}</span></h4>
                    <div class="col-md-12">

                        <p>{!! $new->description !!}

                            <a href="{{$new->url}}" class="btn-info btn-small" target="_blank">Read More</a>

                        </p>
                    </div>
                </div>

        @endforeach



            </div>
        {!! $news->render() !!}

    </div>
    </section>

@endsection


