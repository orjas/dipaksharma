@extends('official.layouts.main')

@section('title')
    interview
@endsection
@section('section')
    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>
    <section id="gallery">
    <div class="container col-md-12">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><i class="fa fa-video-camera"></i> Interview </h2>
                <hr class="my-4">
            </div>
            @foreach($videos as $video)

            <div class="col-md-6 mt-3">
                <strong>{{$video->title}}</strong>
                <br>
                <iframe width="100%" height="360" src="https://www.youtube.com/embed/{{$video->video}}" frameborder="0" gesture="media" allowfullscreen></iframe>
            </div>

            @endforeach





        </div>
        {!! $videos->render() !!}
    </div>
</section>
@endsection


