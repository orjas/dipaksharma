@extends('official.layouts.main')

@section('title')
    Profile
@endsection
@section('section')


    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>

    <section id="gallery">
        <div class="container container-fluid">
            <div class="page-header">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><i class="fa "></i>My Profile </h2>
                    <hr class="my-4">
                    </div>
                    <div class="media hidden-small"></div>



                        <div class="col-lg-12 col-xs-4">

{!! $profile->description !!}

                        </div>

                </div>
            </div>


        </section>





    <section id="bio">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <h2 class="section-heading">{{$header->footer_heading}}</h2>
                    <hr class="my-4">
                    <p class="mb-5">{!!  $header->footer_description!!}</p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 ml-auto text-center">
                    <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                    <p><a href="tel:{{$header->phone}}">{{$header->phone}}</a></p>
                </div>
                <div class="col-lg-4 mr-auto text-center">
                    <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                    <p>
                        <a href="mailto:{{$header->email}}">{{$header->email}}</a>
                    </p>
                </div>
            </div>
        </div>
    </section>

@endsection

