@extends('official.layouts.main')
@section('title')
    Bio
    @endsection
@section('section')

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>
<?php $count=0;?>
    <section id="bio">
    <div class="container">
        <div class="page-header">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading"><i class="fa fa-clock-o"></i>Biography </h2>
                <hr class="my-4">

            </div>
        </div>
        <ul class="timeline">
            @foreach($bios as $bio)
           @if($count%2==0)
            <li>
                <div class="timeline-badge "><i class="fa fa-calendar"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">{{$bio->name}}</h4>
                        <p><small class="text-muted"><i class="fa fa-clock-o"></i> {{$bio->date}}</small></p>
                    </div>
                    <div class="timeline-body">
                        <p>{!! $bio->description !!}</p>
                    </div>
                </div>
            </li>
            @else

            <li class="timeline-inverted">
                <div class="timeline-badge warning"><i class="fa fa-calendar"></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title">{{$bio->name}}</h4>
                        <p><small class="text-muted"><i class="fa fa-clock-o"></i> {{$bio->date}}</small></p>

                    </div>
                    <div class="timeline-body">
                        <p>{!! $bio->description !!}</p>
                    </div>
                </div>
            </li>
                @endif
                <?php $count++;?>
            @endforeach


        </ul>
    </div>
</section>
@endsection


