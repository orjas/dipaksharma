@extends('official.layouts.main')
@section('title')
    Images
@endsection
@section('section')

    <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>

    <section id="gallery" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><i class="fa fa-camera"></i>Gallery </h2>
                    <hr class="my-4">



                <div class="row no-gutters popup-gallery" id="portfolio">
                    @foreach($gallerys as $gallery)

                        <div class="mt-2 col-md-4 ">
                            <a class="portfolio-box" href="{{url(asset('assest/images/gallery/'.$gallery->image))}}">
                                <img  src="{{url(asset('assest/images/gallery/'.$gallery->image))}}" width="365" height="200" alt="">

                            </a>
                            <div class="caption">
                                <p>{!! $gallery->description !!}</p>
                            </div>

                        </div>

                    @endforeach

                </div>
                    {!! $gallerys->render() !!}
        </div>
            </div>
        </div>
    </section>





@endsection
