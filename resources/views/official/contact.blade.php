@extends('official.layouts.main')
@section('title')
    Contact
@endsection
@section('section')

        <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-shrink" id="exceptHomeNav" >
        @include('official.includes.nav')

    </nav>

    <div class="jumbotron jumbotron-sm" style="padding-top: 5em;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <h1 class="h1">
                        Contact me</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="well well-sm">
                    <form action="{{url('contact')}}" method="post">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name">
                                        Name</label>
                                    <input type="text" class="form-control" name='name' id="name" placeholder="Enter name"  />
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">
                                        Email Address</label>
                                    <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i>
                                </span>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email"  /></div>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

<br><br><br>


                            </div>
                            <div class="col-md-6">
                                <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
                                    <label for="name">
                                        Message</label>
                                    <textarea name="message" id="message"  name='message'class="form-control" rows="9" cols="25"
                                              placeholder="Message"></textarea>
                                    @if ($errors->has('message'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>
                            <div class="col-md-12">
                                {!! app('captcha')->display() !!}
                                {{--<div class="g-recaptcha" data-sitekey="{{(config('googlekey.key'))}}"></div>--}}

                                  <div>
                                      @if ($errors->has('g-recaptcha-response'))

                                        <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>

                                    @endif

                                  </div>


                                <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                    Send Message</button>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="social-links-cover">
                            <p><span class="firstWord">Connect </span>with us:</p>

                            @if($setting->facebook_link)
                            <a href="{{$setting->facebook_link}}"  style="color: #007bff;" target="_blank" ><i class=" fa fa-facebook-square fa-lg" aria-hidden="true"></i>
                            </a>&nbsp;
                            @endif
                            @if($setting->youtube)

                            <a href="{{$setting->youtube}}"target="_blank" style="color: #007bff;"><i class="fa fa-youtube fa-lg" aria-hidden="true"></i>
                            </a>&nbsp;
                            @endif
                            @if($setting->twitter)
                            <a href="{{$setting->twitter}}" target="_blank" id="twitter" style="color: #007bff;"><i class="fa fa-twitter fa-lg" aria-hidden="true" ></i>
                            </a>&nbsp;
                            @endif
                            @if($setting->google)
                            <a href="{{$setting->google}}"target="_blank" id="google" style="color: #007bff;"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i>
                            </a>&nbsp;
                            @endif


                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-4">
                <br>
                    <div class="fb-page" data-href="{{$setting->facebook_page}}?ref=bookmarks"
                         data-tabs="timeline"  data-height="350px" data-small-header="false"
                         data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/FirstVisit/?ref=bookmarks"
                                    class="fb-xfbml-parse-ignore"></blockquote>
                    </div>

        </div>

        {{--<div class="col-lg-12">--}}

    </div>



</div>
    <script>
        var google="{{$setting->google}}";
        var twitter="{{$setting->twitter}}";
        console.log(google);
        if(google==""){
            $('#google').hide();
        }
    </script>

@endsection


