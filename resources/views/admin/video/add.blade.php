@extends('admin.layouts.main')


@section('content')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <div class="col-md-offset-9">
        <a href="{{url('admin/video/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/video/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" class="form-control" name="title">
            @if ($errors->has('title'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group">
            <label>Select a Category</label>
            <select name="category" class="form-control">
                <option value="Gallery">Gallery</option>
                <option value="Interview">Interview</option>
            </select>
        </div>

    <div class="form-group {{ $errors->has('video') ? ' has-error' : '' }}">
        <label>Video URL</label>
        <input type="text" class="form-control" name="video">
        @if ($errors->has('video'))
            <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
        @endif
    </div>

        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection
