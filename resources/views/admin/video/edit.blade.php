@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/video/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    {!! Form::open(['url' => 'admin/video/'.$videos->id.'/update', 'method' => 'post']) !!}

    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label >Title</label>
            <input type="text" class="form-control" name="title" value="{{$videos->title}}">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif

    </div>
    <div class="form-group">
        <label>Select a Category</label>
        <select name="category" class="form-control">
            <option value="{{$videos->category}}">{{$videos->category}}</option>
            <option value="Gallery">Gallery</option>
            <option value="Interview">Interview</option>
        </select>
    </div>


    <div class="form-group {{ $errors->has('video') ? ' has-error' : '' }}">
        <label >Video URL</label>
        <input type="text" class="form-control" name="video" value="{{$videos->url}}">
        @if ($errors->has('video'))
            <span class="help-block">
                                        <strong>{{ $errors->first('video') }}</strong>
                                    </span>
        @endif

    </div>


    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>

    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection




