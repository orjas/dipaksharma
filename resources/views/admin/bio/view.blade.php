@extends('admin.layouts.main')


@section('content')

    <div class="col-md-1">
        <a href="{{url('admin/bio/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class=" container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bios as $bio)
                    <tr>
                        <td>{{$bio->name}}</td>
                        <td>{!!  $bio->description!!}</td>
                        <td>{{$bio->date}}</td>
                       <td>
                            <a href={{url('/admin/bio/'.$bio->id.'/edit')}}><button class="btn btn-primary ">Edit</button></a><br><br>
                            <form method="GET" action={{url('admin/bio/'.$bio->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $bios->render()!!}
        </div>
    </div>

@endsection

