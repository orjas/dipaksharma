@extends('admin.layouts.main')


@section('content')
    <div class="col-md-offset-9">
        <a href="{{url('admin/bio/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    {!! Form::open(['url' => 'admin/bio/'.$bios->id.'/update', 'method' => 'post']) !!}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label >Name</label>
            <input type="text" class="form-control" name="name" value="{{$bios->name}}">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
        </div>
    <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
        <label >Date</label>
        <input type="date" class="form-control" name="date" value="{{$bios->date}}">
        @if ($errors->has('date'))
            <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
        @endif
    </div>

        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label>Description</label>
            <textarea name="description" class="form-control simpleEditor">{!! $bios->description !!}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif
        </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



