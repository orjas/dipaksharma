@extends('admin.layouts.main')



@section('content')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <div class="col-md-offset-9">
        <a href="{{url('admin/bio/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <div class="container container-fluid col-md-12">
    <form method="post" action="{{url('admin/bio/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" class="form-control" name="name">
            @if ($errors->has('name'))
                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
            <label >Date</label>
            <input type="date" class="form-control" name="date" >
            @if ($errors->has('date'))
                <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
            @endif
        </div>


        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label>Description</label>
        <textarea name="description" class="form-control edit"></textarea>


        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
    </div>
@endsection


