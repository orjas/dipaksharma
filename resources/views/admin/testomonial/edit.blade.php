@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/testomonial/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    {!! Form::open(['url' => 'admin/testomonial/'.$test->id.'/update', 'method' => 'post','files'=>true]) !!}
    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Name</label>
        <input type="text" class="form-control" name="name" value="{{$test->name}}">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('position') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Position</label>
        <input type="text" class="form-control" name="position" value="{{$test->position}}">
        @if ($errors->has('position'))
            <span class="help-block">
                                        <strong>{{ $errors->first('position') }}</strong>
                                    </span>
        @endif
    </div>

    {{ Form::label('Image', null, ['class' => 'control-label']) }}
    <div >
        <img src="{{asset('assest/images/testomonial/'.$test->image)}}" width="120" align="right">
    </div>
    <input type="file" name="image" >
    <br><br>
    <br><br><br><br>
    <div class="form-group {{ $errors->has('link') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Facebook Link</label>
        <input type="text" class="form-control" name="link" value="{{$test->link}}">
        @if ($errors->has('link'))
            <span class="help-block">
                                        <strong>{{ $errors->first('link') }}</strong>
                                    </span>
        @endif
    </div>



    <div class="form-group {{ $errors->has('message') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Message</label>
        <textarea class="form-control simpleEditor" name="message">{!! $test->message !!}</textarea>
    </div>
    @if ($errors->has('message'))
        <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
    @endif


    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>

    {!! Form::close() !!}


@endsection
{!! Form::hidden('name', '', ['id' => 'id']) !!}



