@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/gallery/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/gallery/'.$gallerys->id.'/update', 'method' => 'post','files'=>true]) !!}


        <div class="form-group">
            <label >Image</label>

            <img src="{{asset('assest/images/gallery/'.$gallerys->image)}}" width="120" align="right">


            <input type="file"  name="image">
          </div>
        <br><br><br><br>
    <div class="form-group">
        <label>Select a Category</label>
        <select name="category" class="form-control">
            <option value="{{$gallerys->category}}">{{$gallerys->category}}</option>
            <option value="Gallery">Gallery</option>
            <option value="News">News</option>
        </select>
    </div>
        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label>Description</label>
            <textarea name="description" class="form-control simpleEditor">{!! $gallerys->description !!}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif

        </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
