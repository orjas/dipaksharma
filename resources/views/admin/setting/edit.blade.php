@extends('admin.layouts.main')

@section('content')


    {!! Form::open(['url' => 'admin/setting/'.$settings->id.'/update', 'method' => 'post','files'=>true]) !!}

    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
        <label >Title</label>
        <input type="text" class="form-control" name="title" value="{{$settings->title}}">
        @if ($errors->has('short_desc1'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('facebook_page') ? ' has-error' : '' }}">
        <label>Facebook Page</label>
        <input type="text" class="form-control" name="facebook_page" value="{{$settings->facebook_page}}">

        @if ($errors->has('facebook_page'))
            <span class="help-block">
                                        <strong>{{ $errors->first('facebook_page') }}</strong>
                                    </span>
        @endif
    </div>


    <div class="form-group {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Facebook</label>
        <input type="text" class="form-control" name="facebook_link" value="{{$settings->facebook_link}}">
        @if ($errors->has('facebook_link'))
            <span class="help-block">
                                        <strong>{{ $errors->first('facebook_link') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('youtube') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Youtube </label>
        <input type="text" class="form-control" name="youtube" value="{{$settings->youtube}}">
        @if ($errors->has('youtube'))
            <span class="help-block">
                                        <strong>{{ $errors->first('youtube') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('twitter') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Twitter </label>
        <input type="text" class="form-control" name="twitter" value="{{$settings->twitter}}">
        @if ($errors->has('twitter'))
            <span class="help-block">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('google') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Google </label>
        <input type="text" class="form-control" name="google" value="{{$settings->google}}">
        @if ($errors->has('google'))
            <span class="help-block">
                                        <strong>{{ $errors->first('google') }}</strong>
                                    </span>
        @endif
    </div>

    <br><br> <br><br>

    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>

    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



