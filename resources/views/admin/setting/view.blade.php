@extends('admin.layouts.main')


@section('content')

    {{--<div class="col-md-1">--}}
        {{--<a href="{{url('admin/setting/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>--}}
    {{--</div>--}}
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Facebook Page</th>
                    <th>Facebook </th>
                    <th>Youtube</th>
                    <th>Twitter</th>
                    <th>Google</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($setting as $set)
                    <tr>
                        <td>{{$set->title}}</td>
                        <td>{{$set->facebook_page}}</td>
                        <td>{{$set->facebook_link}}</td>
                        <td>{{$set->youtube}}</td>
                        <td>{{$set->twitter}}</td>
                        <td>{{$set->google}}</td>

                           <td> <a href={{url('/admin/setting/'.$set->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $setting->render()!!}
        </div>
    </div>

@endsection





