@extends('admin.layouts.main')



@section('content')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <div class="col-md-offset-9">
        <a href="{{url('admin/setting/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/setting/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" class="form-control" name="title">
            @if ($errors->has('title'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('facebook_page') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Facebook Page</label>
            <input type="text" class="form-control" name="facebook_page">
            @if ($errors->has('facebook_page'))
                <span class="help-block">
                                        <strong>{{ $errors->first('facebook_page') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('facebook_link') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Facebook </label>
            <input type="text" class="form-control" name="facebook_link">
            @if ($errors->has('facebook_link'))
                <span class="help-block">
                                        <strong>{{ $errors->first('facebook_link') }}</strong>
                                    </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('youtube') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Youtube</label>
            <input type="text" class="form-control" name="youtube">
            @if ($errors->has('youtube'))
                <span class="help-block">
                                        <strong>{{ $errors->first('youtube') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('twitter') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Twitter</label>
            <input type="text" class="form-control" name="twitter">
            @if ($errors->has('twitter'))
                <span class="help-block">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('google') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Google</label>
            <input type="text" class="form-control" name="google">
            @if ($errors->has('google'))
                <span class="help-block">
                                        <strong>{{ $errors->first('google') }}</strong>
                                    </span>
            @endif
        </div>




        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection


