@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/news')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/news/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Title</label>
            <input type="text" class="form-control" name="title">
            @if ($errors->has('title'))
                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
            @endif
        </div>



        <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Date</label>
            <input type="date" class="form-control" name="date">
            @if ($errors->has('date'))
                <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
            @endif
        </div>
        <div class="form-group {{ $errors->has('url') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">News URL</label>
            <input type="text" class="form-control" name="url">
            @if ($errors->has('url'))
                <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
            @endif
        </div>


    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        <label>Description</label>
        <textarea name="description" class="form-control simpleEditor"></textarea>

        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary btn-lg'])  !!}
        </div>
        {!! csrf_field() !!}
    </form>
@endsection

