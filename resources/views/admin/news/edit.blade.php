@extends('admin.layouts.main')

@section('content')
    <div class="col-md-offset-9">
        <a href="{{url('admin/news')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    {!! Form::open(['url' => 'admin/news/'.$news->id.'/update', 'method' => 'post','files'=>true]) !!}

    <div class="form-group {{ $errors->has('title') ? ' has-error' : '' }}">
            <label >Title</label>
            <input type="text" class="form-control" name="title" value="{{$news->title}}">
        @if ($errors->has('title'))
            <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
        @endif
        </div>

        <br><br>
    <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Date</label>
        <input type="date" class="form-control" name="date" value="{{$news->date}}">
        @if ($errors->has('date'))
            <span class="help-block">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
        @endif
    </div>

    <div class="form-group {{ $errors->has('url') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">News URL</label>
        <input type="text" class="form-control" name="url" value="{{$news->url}}">
        @if ($errors->has('url'))
            <span class="help-block">
                                        <strong>{{ $errors->first('url') }}</strong>
                                    </span>
        @endif
    </div

        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label>Description</label>
            <textarea name="description" class="form-control simpleEditor" >{!! $news->description !!}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif
        </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary btn-lg'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



