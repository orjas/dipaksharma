@extends('admin.layouts.main')

@section('content')
    <div class="col-md-offset-9">
        <a href="{{url('admin/header/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    {!! Form::open(['url' => 'admin/header/'.$headers->id.'/update', 'method' => 'post','files'=>true]) !!}

    <div class="form-group {{ $errors->has('short_desc1') ? ' has-error' : '' }}">
            <label >Short Description 1</label>
            <input type="text" class="form-control" name="short_desc1" value="{{$headers->short_desc1}}">
                  @if ($errors->has('short_desc1'))
            <span class="help-block">
                                        <strong>{{ $errors->first('short_desc1') }}</strong>
                                    </span>
        @endif
        </div>

    <div class="form-group {{ $errors->has('short_desc2') ? ' has-error' : '' }}">
        <label>Short Description2</label>
        <input type="text" class="form-control" name="short_desc2" value="{{$headers->short_desc2}}">

    @if ($errors->has('short_desc2'))
            <span class="help-block">
                                        <strong>{{ $errors->first('short_desc2') }}</strong>
                                    </span>
        @endif
    </div>
        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
            <label >Image</label>

            <img src="{{asset('assest/images/header/'.$headers->image)}}" width="150" align="right">


            <input type="file"  name="image">
            @if ($errors->has('image'))
                <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
            @endif
          </div>

    <div class="form-group {{ $errors->has('footer_heading') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Footer Heading</label>
        <input type="text" class="form-control" name="footer_heading" value="{{$headers->footer_heading}}">
        @if ($errors->has('footer_heading'))
            <span class="help-block">
                                        <strong>{{ $errors->first('footer_heading') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('footer_description') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Footer Description </label>
        <textarea class="form-control simpleEditor" name="footer_description">{!! $headers->footer_description !!}</textarea>
        {{--<input type="text" class="form-control " name="footer_description" value="{{$headers->footer_description}}">--}}
        @if ($errors->has('footer_description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('footer_description') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Number </label>
        <input type="text" class="form-control" name="phone" value="{{$headers->phone}}">
        @if ($errors->has('phone'))
            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
        @endif
    </div>
    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
        <label for="exampleInputEmail1">Email </label>
        <input type="email" class="form-control" name="email" value="{{$headers->email}}">
        @if ($errors->has('email'))
            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
        @endif
    </div>



    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>

    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



