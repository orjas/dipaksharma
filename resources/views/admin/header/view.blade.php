@extends('admin.layouts.main')


@section('content')


    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>

                    <th>Short Description 1</th>
                    <th>Short Description 2</th>
                    <th>Footer Heading</th>
                    <th>Footer Description</th>
                    <th>Phone</th>
                    <th>Email</th>

                    <th>Image</th>

                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($headers as $head)
                    <tr>

                        <td>{{$head->short_desc1}}</td>
                        <td>{{$head->short_desc2}}</td>
                        <td>{{$head->footer_heading}}</td>
                        <td>{!! $head->footer_description!!}</td>
                        <td>{{$head->phone}}</td>
                        <td>{{$head->email}}</td>


                        <td><img src="{{asset('assest/images/header/'.$head->image)}}" width="100"></td>
                           <td> <a href={{url('/admin/header/'.$head->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $headers->render()!!}
        </div>
    </div>

@endsection





