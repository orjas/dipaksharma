<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>General</h3>
        <ul class="nav side-menu">

             <li class="nav-link js-scroll-trigger "><a class="{!! Request::segment(2)=='dashboard'?'active':' '!!}" href="{{url('admin/dashboard')}}"><i class="fa fa-link"></i> <span>Dashboard</span></a></li>
             <li class="nav-link js-scroll-trigger "><a class="{!! Request::segment(2)=='header'?'active':' '!!}" href="{{url('admin/header/view')}}"><i class="fa fa-link"></i> <span>Header</span></a></li>
          <li class=" "><a class="{!! Request::segment(2)=='video'?'active':' '!!}" href="{{url('admin/video/view')}}"><i class="fa fa-link"></i> <span>Video</span></a></li>
          <li class=""><a class="{!! Request::segment(2)=='gallery'?'active':' '!!}"href="{{url('admin/gallery/view')}}"><i class="fa fa-link"></i> <span>Gallery</span></a></li>
          {{--<li class=""><a class="{!! Request::segment(2)=='bio'?'active':' '!!}" href="{{url('admin/bio/view')}}"><i class="fa fa-link"></i> <span>Bio</span></a></li>--}}
          <li class=""><a  class="{!! Request::segment(2)=='works'?'active':' '!!}"href="{{url('admin/works/view')}}"><i class="fa fa-link"></i> <span>Work</span></a></li>
          <li class=""><a  class="{!! Request::segment(2)=='news'?'active':' '!!}"href="{{url('admin/news')}}"><i class="fa fa-link"></i> <span>News</span></a></li>
          <li class=""><a class="{!! Request::segment(2)=='testomonial'?'active':' '!!}"href="{{url('admin/testomonial/view')}}"><i class="fa fa-link"></i> <span>Testomonial</span></a></li>
          <li class=""><a class="{!! Request::segment(2)=='setting'?'active':' '!!}"href="{{url('/admin/setting/edit')}}"><i class="fa fa-link"></i> <span>Setting</span></a></li>
          <li class=""><a class="{!! Request::segment(2)=='profile'?'active':' '!!}"href="{{url('admin/profile/edit')}}"><i class="fa fa-link"></i> <span>Profile</span></a></li>


          <li class=""><a class="{!! Request::segment(2)=='message'?'active':' '!!}"href="{{url('admin/message')}}"><i class="fa fa-link"></i> <span>Message</span></a></li>

        </ul>
    </div>


</div>