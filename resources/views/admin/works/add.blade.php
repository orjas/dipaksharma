@extends('admin.layouts.main')


@section('content')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <div class="col-md-offset-9">
        <a href="{{url('admin/works/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <form method="post" action="{{url('admin/works/add')}}" enctype="multipart/form-data">
        <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" class="form-control" name="name">
            @if ($errors->has('name'))
                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
            @endif
        </div>
        {!! csrf_field() !!}
        <div class="form-group {{ $errors->has('image') ? ' has-error' : '' }}">
            <label >Image</label>
            <input type="file" class="form-control" name="image">
            @if ($errors->has('image'))
                <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
            @endif
        </div>



    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
        <label>Description</label>
        <textarea name="description" class="form-control simpleEditor"></textarea>

        @if ($errors->has('description'))
            <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
        @endif
    </div>

        <div class="text-center">
            {!!  Form::submit('Submit', ['class' => 'btn btn-primary'])  !!}
        </div>

    </form>
@endsection

