@extends('admin.layouts.main')

@section('content')
    <div class="col-md-offset-9">
        <a href="{{url('admin/works/view')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>

    {!! Form::open(['url' => 'admin/works/'.$works->id.'/update', 'method' => 'post','files'=>true]) !!}

    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
            <label >Name</label>
            <input type="text" class="form-control" name="name" value="{{$works->name}}">
        @if ($errors->has('name'))
            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
        @endif
        </div>
        <div class="form-group">
            <label >Image</label>

            <img src="{{asset('assest/images/works/'.$works->image)}}" width="150" align="right">


            <input type="file"  name="image">
          </div>
        <br><br>

        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label>Description</label>
            <textarea name="description" class="form-control simpleEditor">{!! $works->description !!}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif
        </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>
        {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection



