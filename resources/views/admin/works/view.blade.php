@extends('admin.layouts.main')


@section('content')

    <div class="col-sm-3">
        <a href="{{url('admin/works/add')}}" ><button class="btn btn-primary btn-lg" >ADD</button></a>
    </div>
    <div class="col-md-12">
        <div class="container-fluid">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>

                    <th>Description</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($works as $work)
                    <tr>
                        <td>{{$work->name}}</td>

                        <td>{!!  $work->description!!}</td>
                        <td><img src="{{asset('assest/images/works/'.$work->image)}}" width="100"></td>
                        <td>
                            <form method="GET" action={{url('admin/works/'.$work->id.'/delete')}} accept-charset="UTF-8"><input name="_method" type="hidden" value="DELETE"><input name="_token" type="hidden" value="739yZqyJOtUuhACXEVv3MTdAniXfxWBHP3q2lDSq">
                                <button type="button" class="btn btn-danger " href="#"
                                        data-toggle="modal" data-target="#confirmDelete"><i class="fa fa-window-close-o" aria-hidden="true"></i>
                                    Delete</button>
                            </form>

                            {{--<a href={{url('admin/works/'.$work->id.'/delete')}}><button class="btn btn-danger" onclick="return confirm('Are You Sure?');">Delete</button></a><br><br>--}}
                            <a href={{url('/admin/works/'.$work->id.'/edit')}}><button class="btn btn-primary">Edit</button></a>
                        </td>
                    </tr>


                @endforeach
                </tbody>
            </table>
            {!!  $works->render()!!}
        </div>
    </div>

@endsection


