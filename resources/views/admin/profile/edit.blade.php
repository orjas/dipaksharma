@extends('admin.layouts.main')


@section('content')

    <div class="col-md-offset-9">
        <a href="{{url('admin/profile/edit')}}" ><button class="btn btn-primary btn-lg" >Back</button></a>
    </div>
    <br><br>
    {!! Form::open(['url' => 'admin/profile/'.$profile->id.'/update', 'method' => 'post','files'=>true]) !!}
    {{--<div class="form-group">--}}
        {{--<label >Image</label>--}}

        {{--<img src="{{asset('assest/images/profile/'.$profile->image)}}" width="120" align="right">--}}


        {{--<input type="file"  name="image">--}}
    {{--</div>--}}
    {{--<br><br><br>--}}

        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
            <label>Description</label>
            <textarea name="description" cols='15' class="form-control" id="editor">{!! $profile->description !!}</textarea>
            @if ($errors->has('description'))
                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
            @endif

        </div>
    <div class="text-center">
        {!!  Form::submit('Update', ['class' => 'btn btn-primary'])  !!}
    </div>

    {!! csrf_field() !!}
    {!! Form::close() !!}

@endsection
