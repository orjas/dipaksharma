<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=\Faker\Factory::create();
        Model::unguard();

        // $this->call(UserTableSeeder::class);
        \App\User::create([
            'name' => 'test',
            'email' => 'test@gmail.com',
            'password' => bcrypt('password'),

        ]);
        \App\Setting::create([
            'title'=>$faker->name,
            'facebook_page'=>$faker->domainName,
            'facebook_link'=>$faker->domainWord,
            'youtube'=>$faker->name,
            'twitter'=>$faker->name,
            'google'=>$faker->name,



        ]);

        Model::reguard();
    }
}
